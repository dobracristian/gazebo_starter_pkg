#ifndef _FACTORY_HH
#define _FACTORY_HH

#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
 {

	class Factory : public WorldPlugin
	{       
                /// \brief Load
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);
		
	};
 }
#endif

#ifndef _HELLO_WORLD_HH
#define _HELLO_WORLD_HH

///The necessary headers we need
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"

///Plugins must be in gazebo namespace
namespace gazebo
 {
	/// \Class HelloWorld hello_world.hh
	/// \brief Class for printing out a "Hello World" using a World Plugin.(Each plugin must be inherit from a plugin type)
	class HelloWorld: public WorldPlugin
	{       
		/// \brief The World Plugin's Load function which receives sdf and physics arguments
		public: void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
	};
 }
#endif


#ifndef _CORK_SIMULATION_HH
#define _CORK_SIMULATION_HH

#include "boost/bind.hpp"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "stdio.h"

namespace gazebo
 {

	class CorkSimulation: public WorldPlugin
	{
		/// \brief Load
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

		/// \Called by the world update start event
		public: void OnUpdate();

		/// \brief Standard physics onject1.
		public:physics::ModelPtr object1;

		/// \brief Standard physics onject1.
		public:physics::ModelPtr object2;

		/// \brief double break_limit.
		public: double break_limit;

		/// \brief Standard physics myJoint.
		public:	physics::JointPtr myJoint;

		/// \brief Standard physics engine.
		public:	physics::PhysicsEnginePtr engine;

		/// \brief Standard math pose.
		private: math::Pose jointPose;

		/// \Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;

		/// \Pointer to the model
		private: physics::WorldPtr world;

	}; 
 }
#endif

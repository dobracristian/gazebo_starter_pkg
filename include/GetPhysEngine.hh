#ifndef _GET_PHYS_ENGINE_HH
#define _GET_PHYS_ENGINE_HH

///The necessary headers we need
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"

///Plugins must be in gazebo namespace
namespace gazebo
{
 	/// \Class GetPhyEngine get_phy_engine.hh
 	/// \brief Class for printing out the physics engine type using a World Plugin.(Each plugin must be inherit from a plugin type)
	class GetPhysEngine: public WorldPlugin
	{
		/// \brief The World Plugin's Load function which receives sdf and physics arguments
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		/// \brief Standard physics engine. Pointer to the engine
		private: physics::PhysicsEnginePtr engine;

		/// \brief Standard physics world.Pointer to the world
		private: physics::WorldPtr world;
	};
}
#endif

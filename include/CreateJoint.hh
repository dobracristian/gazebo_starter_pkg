#ifndef _CREATE_JOINT_HH
#define _CREATE_JOIN_HH

#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"

namespace gazebo {
/// \Class CreateJoint create_joint.hh
	class CreateJoint: public WorldPlugin

	{
		/// \brief Load.
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		/// \brief Init.
		public: void Init();

		/// \brief OnUpdate.
		public: void OnUpdate();

		/// \brief Standard physics world.
		public:physics::WorldPtr world;
		
		/// \brief Standard physics onject1.
		public:physics::ModelPtr object1;
		
		/// \brief Standard physics onject2.
		public:	physics::ModelPtr object2;
		
		/// \brief Standard connection load World.
		public:	event::ConnectionPtr loadWorld;
		
		/// \brief Standard physics myJoint.
		public:	physics::JointPtr myJoint;
		
		/// \brief Standard physics engine.
		public:	physics::PhysicsEnginePtr engine;
		
		/// \brief Standard math pose.
		public:	math::Pose jointPose;
		
		/// \brief A static zero time variable set to common::Time(0, 0).
		public:	common::Time prevWallTime;
		
		/// \brief Min value flag
		public:	bool flagMin;

		/// \brief Max value flag
		public:	bool flagMax;

 };
}
#endif

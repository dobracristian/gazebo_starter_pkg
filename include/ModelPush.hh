#ifndef _MODEL_PUSH_HH
#define _MODEL_PUSH_HH

#include "boost/bind.hpp"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "stdio.h"

namespace gazebo
 {

	class ModelPush: public ModelPlugin
	{       
		/// \brief Load
		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);

		/// \Called by the world update start event
		public: void OnUpdate(const common::UpdateInfo & /*_info*/);

		/// \Pointer to the model
		private: physics::ModelPtr model;

		/// \Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;

		/// \brief Math vector.
		gazebo::math::Vector3 vector;

	};
 }
#endif

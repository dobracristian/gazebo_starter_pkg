#ifndef SPAWN_UNITS_PLUGIN_HH
#define SPAWN_UNITS_PLUGIN_HH

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>

namespace sim_games
{

/// \brief class SpawnUnits
class SpawnUnits : public gazebo::WorldPlugin
{
    /// \brief Constructor
    public: SpawnUnits();

    /// \brief Destructor
    public: virtual ~SpawnUnits();

    /// \brief Load plugin
    protected: virtual void Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf);

    /// \brief Generate sdf
    private: sdf::SDF GenerateSDF(
            const std::string _model_name,
            const std::string _spawn_geometry,
            const std::string _vis_geometry,
            const double _unit_nr,
            const double _spawn_diam,
            const gazebo::math::Vector3 _spawn_pos,
            const gazebo::math::Vector3 _inertia_vect,
            const double _unit_mass,
            const double _unit_size,
            const double _cfm,
            const double _erp,
            const double _kp,
            const double _kd,
            const std::string _collide_bitmask,
            const std::string _scripts_uri,
            const std::string _textures_uri,
            const std::string _mesh_uri,
            const double _mesh_scale,
            const std::string _script_name,
            const bool _static_flag
            );

    /// \brief Arrange spheres in a circular fashion
    private: gazebo::math::Vector3 ArrangeUnits(
            const int _curr_sphere_index,
            const double _radius,
            const double _spawn_diameter,
            int& _spawned,
            int& _level);

};

}

#endif

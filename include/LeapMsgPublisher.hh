#ifndef _LEAP_MSG_PUBLISHER_HH
#define _LEAP_MSG_PUBLISHER_HH

//The necessary headers we need
#include "gazebo/gazebo.hh"
#include "gazebo/transport/TransportIface.hh"
#include "gazebo/msgs/msgs.hh"
#include "leap_msg.pb.h"

//Plugins must be in gazebo namespace
namespace gazebo {

	 /// \Class LeapMsgPublisher LeapMsgPublisher.hh
	 /// \brief Class for publishing vector3d messages using a World Plugin.(Each plugin must be inherit from a plugin type)
	class LeapMsgPublisher: public WorldPlugin
	{
		// \brief The World Plugin's Load function which receives sdf and physics arguments
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		// \brief The World Plugin's Init function
		public: void Init();

		// \brief The World Plugin's OnUpdate function
		public: void OnUpdate();

		/// \brief Standard connection pointer.Pointer to the loadConnection
 		private: event::ConnectionPtr loadConnection;

 		/// \brief Transport node pointer.
		private: gazebo::transport::NodePtr node;

 		/// \brief Transport publisher pointer.
 		gazebo::transport::PublisherPtr pub;

 		/// \brief Standard double vector message.
 		leap_msg_creator_msgs::msgs::LeapMotionMsg leapMsg;
	};
}

#endif

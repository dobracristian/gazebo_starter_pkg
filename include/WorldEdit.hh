#ifndef _WORLD_EDIT_HH
#define _WORLD_EDIT_HH

#include "sdf/sdf.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"

namespace gazebo
 {

	class WorldEdit : public WorldPlugin
	{       
                /// \brief Load
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

	};
 }
#endif

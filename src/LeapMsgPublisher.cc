#include "LeapMsgPublisher.hh"
//iomanip included for output decimal precision
#include <iomanip>

///linking to the namespace gazebo from CustomMsgPublisher.hh
using namespace gazebo;

//////////////////////////////////////////////////
//The Load function sets up the counter, the updateRate, the prevUpdateTime and the updateConnection
//which were added as class variables in CustomMsgPublisher class
void LeapMsgPublisher::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	//Printing out a message in terminal for checking if we are in Load function
	std::cout << "LeapMsgPublisher: Entering Load()" << std::endl;

}

/////////////////////////////////////////////////
//The Init is called once after Load and sets up the node, the pub and the loadConnection
//which were added as class variables in CustomMsgPublisher class
 void LeapMsgPublisher::Init()
 {
	//Printing out a message in terminal for checking if we are in Init function
	std::cout << "LeapMsgPublisher: Entering Init()" << std::endl;

	this->leapMsg.set_id(2);
	this->leapMsg.set_timestamp(12);
	this->leapMsg.set_hands(1);
	this->leapMsg.set_fingers(5);
	this->leapMsg.set_tools(4);
	this->leapMsg.set_gestures(2);

	//Create a new transport Node, which provides functions to create publishers
 	this->node = transport::NodePtr(new transport::Node());

 	//Initialize the node
 	this->node->Init("EmptyWorld");

 	std::cout << "Request: " <<
 	                     " id:" << leapMsg.id() <<std::endl;
 	                     " timestamp:" << std::setprecision(20)<<leapMsg.timestamp() <<
 	                     " hands:" << leapMsg.hands() <<
 	                     " fingers:" << leapMsg.fingers() <<
 	                     " tools:" << leapMsg.tools() <<
 	                     " gestures:" << leapMsg.gestures() <<std::endl;

 	//Create the publisher on the topic ~/CustomMsg_topic
 	this->pub = node->Advertise<leap_msg_creator_msgs::msgs::LeapMotionMsg>("~/CustomMsg_topic");

 	std::cout << "waiting for connection .. " << std::endl;
 	this->pub->WaitForConnection();
 	std::cout << "waiting for connection  DONE.. " << std::endl;

 	//This event is broadcast every simulation iteration
 	this->loadConnection = event::Events::ConnectWorldUpdateBegin(
 			boost::bind(&LeapMsgPublisher::OnUpdate, this));
 }

 //OnUpdate is called on every update event. See the Load function
 //////////////////////////////////////////////////
 void LeapMsgPublisher::OnUpdate()
 {
	//Printing out a message in terminal for checking if we are in OnUpdate function
//	std::cout << "LeapMsgPublisher: Entering OnUpdate()" << std::endl;

 	//Publish our message
 	this->pub->Publish(leapMsg);

}

GZ_REGISTER_WORLD_PLUGIN(LeapMsgPublisher)

#include <math.h>
#include <sdf/sdf.hh>

#include "gazebo/gazebo.hh"
#include "gazebo/common/common.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/transport/TransportIface.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/msgs/msgs.hh"

#include "leap_msg.pb.h"

#include <iostream>
#include <string.h>
#include "Leap.h"

using namespace Leap;

class SampleListener : public Listener
{
    public:
    virtual void onConnect(const Controller&);
    virtual void onFrame(const Controller&);
    leap_msg_creator_msgs::msgs::LeapMotionMsg leapMsg;
    gazebo::transport::PublisherPtr msgPublisher;
};

void SampleListener::onConnect(const Controller& controller)
{
    std::cout << "Connect" << std::endl;
    controller.enableGesture(Gesture::TYPE_SWIPE);
}

void SampleListener::onFrame(const Controller& controller)
{
	const Frame frame=controller.frame();

	this->leapMsg.set_id(frame.id());
	this->leapMsg.set_timestamp(frame.timestamp());
	this->leapMsg.set_hands(frame.hands().count());
	this->leapMsg.set_fingers(frame.fingers().count());
	this->leapMsg.set_tools(frame.tools().count());
	this->leapMsg.set_gestures(frame.gestures().count());
	this->leapMsg.set_pointables(frame.pointables().count());

	std::cout << "Frame id: " << frame.id()<<std::endl;
          << ", timestamp: " << frame.timestamp()
          << ", hands: " << frame.hands().count()
          << ", fingers: " << frame.fingers().count()
          << ", tools: " << frame.tools().count()
          << ", gestures: " << frame.gestures().count()
          << ", pointables: " << frame.pointables().count()<<std::endl;

    this->msgPublisher->Publish(this->leapMsg);

    std::cout << this->msgPublisher->GetTopic() << std::endl;

}

int main(int argc, char** argv)
{
    Controller controller;
    SampleListener listener;

    	gazebo::transport::init();
        gazebo::transport::run();
        gazebo::transport::NodePtr node(new gazebo::transport::Node());
        node->Init("default");

        std::cout << "a3" << std::endl;

        listener.msgPublisher = node->Advertise<leap_msg_creator_msgs::msgs::LeapMotionMsg>("~/LeapMsg_topic");

        std::cout << "Waiting for connection .. " << std::endl;
        listener.msgPublisher->WaitForConnection();
        std::cout << "Waiting for connection .. DONE" << std::endl;
        controller.addListener(listener);

        listener.msgPublisher->Publish(listener.leapMsg);


    //Keep this process running untill Enter is pressed
    std::cout << "Press Enter to quit..." <<std::endl;
    std::cin.get();

    std::cout << "a5" << std::endl;
    gazebo::transport::fini();

    //Remove the sample listenner when done
    controller.removeListener(listener);

    return 0;
}

#include "HelloWorld.hh"

///linking to the namespace gazebo from GetPhysEngine.hh
using namespace gazebo;

//////////////////////////////////////////////////
///The Load function printing out a "Helo World"
void HelloWorld::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
            {	
		///Printing out a message in terminal for checking if we are in Load function
		std::cout<<"HelloWorld: In Load function"<<std::endl;
				///Printing out the "Hello World" message in terminal
    	        std::cout<<"Hello World"<<std::endl;
            }

GZ_REGISTER_WORLD_PLUGIN(HelloWorld)


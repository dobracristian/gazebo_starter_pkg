#include "SystemSubscriber.hh"

using namespace gazebo;


//////////////////////////////////////////////////
void SystemSubscriber::Load(int /*_argc*/, char ** /*_argv*/)
{	std::cout << "SystemSubscriber: Entering Load()" << std::endl;
	this->sceneNode = NULL;

	this->entity = NULL;
}


void SystemSubscriber::Init()
{	std::cout << "SystemSubscriber: Entering Init()" << std::endl;
	this->updateConnection = event::Events::ConnectPreRender(
			boost::bind(&SystemSubscriber::OnUpdate, this));
}


//////////////////////////////////////////////////
void SystemSubscriber::OnUpdate()
{	std::cout << "SystemSubscriber: Entering OnUpdate()" << std::endl;
	this->manager = gui::get_active_camera()->GetScene()->GetManager();
	this->sceneNode = this->manager->getRootSceneNode()->createChildSceneNode(
			"MySphere");
	this->entity = this->manager->createEntity("MySphere",
			Ogre::SceneManager::PT_SPHERE);
	this->entity->setMaterialName("Gazebo/Red");
	this->sceneNode->setVisible(true);
	this->entity->setVisible(true);

	this->sceneNode->setScale(0.001, 0.001, 0.001);

	this->sceneNode->setPosition(0, 1, 1);

	this->sceneNode->attachObject(entity);
	this->CreateSubscriber();
	this->updateConnection.reset();
}

//////////////////////////////////////////////////
void SystemSubscriber::CreateSubscriber()
{	std::cout << "SystemSubscriber: Entering CreateSubscriber()" << std::endl;

	this->node = transport::NodePtr(new transport::Node());
	this->node->Init("EmptyWorld");

	this->sub = node->Subscribe("~/sphere_topic",
			&SystemSubscriber::MsgCb, this);
	std::cout << "Subscribing to topic : ~/sphere_topic" << std::endl;

}


/////////////////////////////////////////////////
void SystemSubscriber::MsgCb(const boost::shared_ptr<const gazebo::msgs::Vector3d> &_msg)

{	std::cout << "SystemSubscriber: Entering MsgCb()" << std::endl;

	std::cout << _msg->DebugString() << std::endl;
	this->sceneNode->setPosition(_msg->x(), _msg->y(), _msg->z());

}

GZ_REGISTER_SYSTEM_PLUGIN(SystemSubscriber)

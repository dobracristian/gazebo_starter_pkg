#include "CustomMsgPublisher.hh"

///linking to the namespace gazebo from CustomMsgPublisher.hh
using namespace gazebo;

//////////////////////////////////////////////////
//The Load function sets up the counter, the updateRate, the prevUpdateTime and the updateConnection
//which were added as class variables in CustomMsgPublisher class
void CustomMsgPublisher::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	//Printing out a message in terminal for checking if we are in Load function
	std::cout << "CustomMsgPublisher: Entering Load()" << std::endl;
	//Counter is intialized to 0
	this->counter = 0;

	//updateRate is set to 1s:0ms
	this->updateRate = common::Time(1, 0);

	//prevUpdateRate is set to last time when was the updateRate done
	this->prevUpdateTime = common::Time::GetWallTime();
	
}

//The Init is called once after Load and sets up the node, the pub and the loadConnection
//which were added as class variables in CustomMsgPublisher class
/////////////////////////////////////////////////
 void CustomMsgPublisher::Init()
 {	
	//Printing out a message in terminal for checking if we are in Init function
	std::cout << "CustomMsgPublisher: Entering Init()" << std::endl;

	//Create a new transport Node, which provides functions to create publishers
 	this->node = transport::NodePtr(new transport::Node());

 	//Initialize the node
 	this->node->Init("EmptyWorld");

 	//Create the publisher on the topic ~/CustomMsg_topic
 	this->pub = node->Advertise<gazebo::msgs::Vector3d>("~/CustomMsg_topic");

 	//This event is broadcast every simulation iteration
 	this->loadConnection = event::Events::ConnectWorldUpdateBegin(
 			boost::bind(&CustomMsgPublisher::OnUpdate, this));
 }

 //OnUpdate is called on every update event. See the Load function
 //////////////////////////////////////////////////
 void CustomMsgPublisher::OnUpdate()
 { 	
	//Printing out a message in terminal for checking if we are in OnUpdate function
	std::cout << "CustomMsgPublisher: Entering OnUpdate()" << std::endl;
	//checking if the time differece between the last updateRade done and current update is 0 or not
 	if (common::Time::GetWallTime() - this->prevUpdateTime < this->updateRate)
 		return;
 	//Set up the vector which is added as class in CustomPublisher class
 	this->vector = gazebo::math::Vector3(this->counter, 1, 1);

 	//initializes the msg with the vector
 	gazebo::msgs::Set(&msg, vector);

 	//Publish our message
 	pub->Publish(msg);

 	//increment counter by 0.1
 	this->counter = counter + 0.1;

 	//prevUpdateRate is set to last time when was the updateRate done
 	this->prevUpdateTime = common::Time::GetWallTime();
}

GZ_REGISTER_WORLD_PLUGIN(CustomMsgPublisher)

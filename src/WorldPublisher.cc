#include "WorldPublisher.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void WorldPublisher::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
{	std::cout << "WorldPublisher: Entering Load()" << std::endl;
	this->counter = 0;
	this->updateRate = common::Time(1, 0);
	this->prevUpdateTime = common::Time::GetWallTime();
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&WorldPublisher::OnUpdate, this));
}

 ////////////////////////////////////////////////
 void WorldPublisher::Init()
 {	std::cout << "WorldPublisher: Entering Init()" << std::endl;
 	this->node = transport::NodePtr(new transport::Node());
 	this->node->Init("EmptyWorld");
 	this->pub = node->Advertise<gazebo::msgs::Vector3d>("~/WorldMsg_topic");
 	this->loadConnection = event::Events::ConnectWorldUpdateBegin(
 			boost::bind(&WorldPublisher::OnUpdate, this));
 }

 //////////////////////////////////////////////////
 void WorldPublisher::OnUpdate()
 { 	std::cout << "WorldPublisher: Entering OnUpdate()" << std::endl;
 	if (common::Time::GetWallTime() - this->prevUpdateTime < this->updateRate)
 		return;
 	this->vector = gazebo::math::Vector3(this->counter, 1, 1);
 	gazebo::msgs::Set(&msg, vector);
 	pub->Publish(msg);
 	this->counter = counter + 0.1;
 	this->prevUpdateTime = common::Time::GetWallTime();
}
GZ_REGISTER_WORLD_PLUGIN(WorldPublisher)
